package com.example.alexis.evaluacion2;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements DatosPersonalesFragment.OnFragmentInteractionListener, ContactoFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        DatosPersonalesFragment fragment = new DatosPersonalesFragment();
        ft.replace(R.id.contenedor, fragment);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(String nombreFragmento, String evento) {
        if(evento.equals("Click")){
            if(nombreFragmento.equals("DatosPersonalesFragment")){
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                ContactoFragment fragmento = new ContactoFragment();
                ft.replace(R.id.contenedor, fragmento);
                ft.commit();
            }
            if (nombreFragmento.equals("ContactoFragment")){
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                LoginFragment fragmento = new LoginFragment();
                ft.replace(R.id.contenedor, fragmento);
                ft.commit();
            }
        }
    }
}
